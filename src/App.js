import React from "react";
import TestimonialCardPage from "./pages/TestimonialCardPage";
import CollapseCardPage from "./pages/CollapseCardPage";
import "./static/main.scss";

function App() {
  return (
    <div className="App">
      {/* <TestimonialCardPage /> */}
      <CollapseCardPage />
    </div>
  );
}

export default App;
