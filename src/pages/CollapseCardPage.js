import React, { Component } from "react";
import CollapseCard from "../components/CollapseCard";

const collapseCardData = [
  {
    attribute: "quality",
    mainHead: "OUR GOODS",
    desc: "are delivered with the same high quality as decades ago",
  },
  {
    attribute: "label",
    mainHead: "OUR GOODS",
    desc: "are delivered with the same high quality as decades ago",
  },
  {
    attribute: "quality",
    mainHead: "OUR GOODS",
    desc: "are delivered with the same high quality as decades ago",
  },
];

class CollapseCardPage extends Component {
  render() {
    return (
      <div className="collapse__card__section">
        <div className="container">
          <div className="row">
            {collapseCardData.map((data, index) => (
              <div className="col-xl-4">
                <CollapseCard
                  key={index}
                  attribute={data.attribute}
                  mainText={data.mainHead}
                  secText={data.desc}
                />
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default CollapseCardPage;
