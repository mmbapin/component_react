import React, { Component } from "react";
import TestimonialCard from "../components/TestimonialCard";

const testimonialData = [
  {
    img: "/images/testimonial_card/img.png",
    desc:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsumcumque culpa totam magni illo enim quis soluta, eum eos nobis,impedit blanditiis cupiditate corrupti, debitis inventore quidistinctio nesciunt fugit",
    userImg: "/images/testimonial_card/user1.jpg",
    uName: "John Doe",
  },
  {
    img: "/images/testimonial_card/img.png",
    desc:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsumcumque culpa totam magni illo enim quis soluta, eum eos nobis,impedit blanditiis cupiditate corrupti, debitis inventore quidistinctio nesciunt fugit",
    userImg: "/images/testimonial_card/user2.jpg",
    uName: "Maximilan",
  },
  {
    img: "/images/testimonial_card/img.png",
    desc:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsumcumque culpa totam magni illo enim quis soluta, eum eos nobis,impedit blanditiis cupiditate corrupti, debitis inventore quidistinctio nesciunt fugit",
    userImg: "/images/testimonial_card/user3.jpg",
    uName: "Jason Joe",
  },
];

class TestimonialCardPage extends Component {
  render() {
    return (
      <div className="testimonial_card_section">
        <div className="container">
          <div className="row">
            {testimonialData.map((data, index) => (
              <div className="col-xl-4">
                <TestimonialCard
                  key={index}
                  image={data.img}
                  description={data.desc}
                  userImage={data.userImg}
                  userName={data.uName}
                />
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default TestimonialCardPage;
