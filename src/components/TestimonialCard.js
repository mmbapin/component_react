import React, { Component } from "react";

class TestimonialCard extends Component {
  render() {
    const { image, description, userImage, userName } = this.props;
    return (
      <React.Fragment>
        <div className="card testimonial_card">
          <img className="card-img-top" src={image} alt="quote image" />
          <div className="card-body">
            <p className="desc_text">{description}</p>
            <img src={userImage} alt="userImage" className="user_img" />
            <h3 className="card-title">{userName}</h3>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default TestimonialCard;
